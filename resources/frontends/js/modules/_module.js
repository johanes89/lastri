import Vue from 'vue'

import Spinner from "vue-simple-spinner";
import { BootstrapVue, BModal, ModalPlugin, FormFilePlugin  } from 'bootstrap-vue'
import Toasted from 'vue-toasted';
import Swal from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.component('pagination', require('laravel-vue-pagination'));


const optionsToasted = {
    iconPack:'fontawesome',
    duration:5000,
    position:'top-right',
    keepOnHover: true,
  };

Vue.use(Toasted, optionsToasted);

const optionSwal = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674',
    showConfirmButton: true,
    confirmButtonText: 'Yes',
    allowEnterKey: true
  };

Vue.use(Swal, optionSwal);

Vue.use(BootstrapVue);
Vue.use(ModalPlugin);
Vue.use(FormFilePlugin)
Vue.use(require('vue-moment'));
Vue.component('vue-spinner', Spinner);
Vue.component('b-modal', BModal);

/**Example 
 * this.$swal({
      toast:true,
      position:'top-end',
      showConfirmButton: false,
      icon: 'error',
      text: 'Something went wrong!'
    });

    this.$toasted.show('ini pesan', {
      iconPack:'fontawesome',
      icon : 'check',
    duration:5000,
    type:'success',
    position:'top-right'
    });
*/
require('@fortawesome/fontawesome-free/js/all.min');