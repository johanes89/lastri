import Vue from 'vue'
import Vuex from 'vuex'
import $axios from '../modules/config.js'

Vue.use(Vuex)

const auth = new Vuex.Store({
    state: {
        token: localStorage.getItem('userToken'),
        errors: [],
        buffer: localStorage.getItem('app-buffer'),
        userId: localStorage.getItem('app-state'),
        clientId: localStorage.getItem('app-client'),
        activeContactMessage: localStorage.getItem("activeContactMessage"),
        stateButton: false,
        userData: [],
        settingDate: true,
    },
    getters: {
        isAuth: state => {
            return state.token != 'null' && state.token != null && state.token != 'undefined'
        },
        isCandidate: state => {
            return state.buffer == 'true' || state.buffer == true
        }
    },

    actions: {
        checkLogin({ commit }, payload) {
            localStorage.setItem('userToken', null);
            commit('setToken', null, { root: true })

            return new Promise((resolve, reject) => {
                $axios
                    .post('api/checkLogin', payload)
                    .then((response) => {
                        if (response.data.status == 'success') {
                            localStorage.setItem('userToken', response.data.userData.token)
                            localStorage.setItem('app-state', response.data.userData.id)
                            localStorage.setItem('app-client', response.data.userData.clientId)
                            localStorage.setItem('app-buffer', response.data.userData.isCandidate)
                            commit('setUserId', response.data.userData.id, { root: true })
                            commit('setClientId', response.data.userData.clientId, { root: true })
                            commit('setToken', response.data.userData.token, { root: true })
                            commit('setBuffer', response.data.userData.isCandidate, { root: true })
                            commit('setUserData', response.data.userData, { root: true })
                            commit('setStateButton', false, { root: true });
                        } else {
                            commit('setErrors', { invalid: response.data.status }, { root: true })
                            commit('setStateButton', false, { root: true });
                        }
                        resolve(response.data)
                    })
                    .catch((error) => {
                        if (error.response.status == 422) {
                            commit('setErrors', error.response.data.errors, { root: true });
                            commit('setStateButton', false, { root: true });
                        }
                    })
            })
        },
        logOut({ commit }) {
            axios
                .post("api/logout")
                .then(function (response) {
                    if (response.data.status == 'success') {
                        localStorage.removeItem("userToken");
                        localStorage.removeItem("app-state");
                        localStorage.removeItem("app-buffer");
                        localStorage.removeItem("activeContactMessage");
                        commit('setToken', null, { root: true });
                        commit('setUserId', null, { root: true });
                        commit('setBuffer', null, { root: true });
                        commit('clearUserData', { root: true });
                        commit('setActiveContactMessage', null, { root: true });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        selectContact({ commit }, value) {
            localStorage.setItem("activeContactMessage", value);
            commit("setActiveContactMessage", value, { root: true });
        },
        setUserData({ commit }, value) {
            commit('setUserData', value, { root: true })
        },
        setSettingDate({ commit }, value) {
            commit('setSettingDate', value, { root: true })
        }
    },
    mutations: {
        setToken(state, payload) {
            state.token = payload;
        },
        setErrors(state, payload) {
            state.errors = payload;
        },
        clearErrors(state) {
            state.errors = [];
        },
        clearUserData(state) {
            state.userData = [];
        },
        setUserId(state, payload) {
            state.userId = payload;
        },
        setClientId(state, payload) {
            state.clientId = payload;
        },
        setBuffer(state, payload) {
            state.buffer = payload;
        },
        setUserData(state, payload) {
            state.userData = payload;
        },
        setActiveContactMessage(state, payload) {
            state.activeContactMessage = payload;
        },
        setStateButton(state, value) {
            state.stateButton = value;
        },
        setSettingDate(state, value) {
            state.settingDate = value;
        }
    },
})

export default auth