<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Nama</label>
            {{ Form::text(
                'name',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Name',
                    'required' => true
                    )
            ) }}
        </div>
        @if ($errors->has('name'))
            <span class="form-note text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Status</label>
            <div class="form-control-wrap ">
                <div class="form-control-select">
                {{ Form::select(
                    'status',
                    ['0' => 'Inactive', '1' => 'Active'],
                    null,
                    array(
                        'class' => 'form-control default-01',
                        'placeholder' => 'Select Status'
                        )
                ) }}
                </div>
                @if ($errors->has('status'))
                    <span class="form-note text-danger">{{ $errors->first('status') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>