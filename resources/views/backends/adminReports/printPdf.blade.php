<!DOCTYPE html>
<html lang="zxx" class="js">

<link href="{{ asset('css/backend-dashlite-packed.css') }}" rel="stylesheet">
<link href="{{ asset('css/modules-asset-packed.css') }}" rel="stylesheet">

<body class="nk-body bg-white npc-default has-aside ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap ">
                
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container wide-xl">
                        <div class="nk-content-inner">
                            
                            <div class="nk-content-body">
                                <div class="nk-content-wrap">
                                    <div class="nk-block-head">
                                        <div class="nk-block-between g-3">
                                            
                                        </div>
                                    </div><!-- .nk-block-head -->
                                    <div class="nk-block">
                                        <div class="invoice">
                                            <div class="invoice-action">
                                                
                                            </div><!-- .invoice-actions -->
                                            <div class="invoice-wrap">
                                                <div class="invoice-brand text-center">
                                                        <img src="{{ asset($configuration->logoUrl) }}" srcset="{{ asset($configuration->smallLogoUrl) }} 2x" alt="">
                                                </div>
                                                <div class="invoice-head">
                                                    <div class="invoice-contact">
                                                        <span class="overline-title">Laporan Transaksi #{{ (isset($transaction->transaction_code)?$transaction->transaction_code:'') }}</span>
                                                        <div class="invoice-contact-info">
                                                            <h4 class="title">{{ (isset($transaction->fullname)?$transaction->fullname:'') }}</h4>
                                                            <ul class="list-plain">
                                                                <li><em class="icon ni ni-map-pin-fill"></em>
                                                                    <span>
                                                                        {{ (isset($transaction->address)?$transaction->address:'') }}, 
                                                                        {{ (isset($transaction->sub_district_id)?$transaction->subDistrict->name:'') }}, 
                                                                        {{ (isset($transaction->district_id)?$transaction->district->name:'') }}, 
                                                                        {{ (isset($transaction->regency_id)?$transaction->regency->name:'') }}, 
                                                                        {{ (isset($transaction->province_id)?$transaction->province->name:'') }}, 
                                                                        {{ (isset($transaction->country_id)?$transaction->country->name:'') }}, 
                                                                        {{ (isset($transaction->postal_code)?$transaction->postal_code:'') }}
                                                                    </span></li>
                                                                <li><em class="icon ni ni-call-fill"></em><span>
                                                                        {{ (isset($transaction->phone)?$transaction->phone:'') }} / 
                                                                        {{ (isset($transaction->email)?$transaction->email:'') }}</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div><!-- .invoice-head -->
                                                <div class="invoice-bills mt-2">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Metode Pembayaran</th>
                                                                        <th>Metode Pengiriman</th>
                                                                        <th>Ekspedisi</th>
                                                                        <th>Biaya Kirim</th>
                                                                        <th>No Resi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>{{ (isset($transaction->payment_method_id)?$transaction->paymentMethod->name:'') }}</td>
                                                                        <td>{{ (isset($transaction->shipping_method_id)?$transaction->shippingMethod->name:'') }}</td>
                                                                        <td>{{ (isset($transaction->expedition)?$transaction->expedition:'') }}</td>
                                                                        <td>{{ (isset($transaction->shipping_cost)?$transaction->shippingCostFormat:'') }}</td>
                                                                        <td>{{ (isset($transaction->receipt_code)?$transaction->receipt_code:'') }}</td>
                                                                        
                                                                    </tr>
                                                                </tbody>
                                                                
                                                            </table>
                                                        </div>
                                                    </div><!-- .invoice-bills -->
                                                <div class="invoice-bills mt-2">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Produk</th>
                                                                    <th>Jumlah</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($transaction->transactionDetail as $product)
                                                                <tr>
                                                                    <td>{{ (isset($product->product_detail_id)?$product->productDetail->productDetailName:'') }}</td>
                                                                    <td>{{ (isset($product->qty)?$product->qty:'') }}</td>
                                                                    <td>{{ (isset($product->total)?$product->totalFormat:'') }}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Subtotal</td>
                                                                    <td>{{ (isset($transaction->grand_total)?$transaction->grandTotalFormat:'') }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Biaya Kirim</td>
                                                                    <td>{{ (isset($transaction->shipping_cost)?$transaction->shippingCostFormat:'') }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>Grand Total</td>
                                                                    <td>{{ (isset($transaction->grand_total)?$transaction->grandTotalAndShippingCostFormat:'') }}</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                        <div class="nk-notes ff-italic fs-12px text-soft"> Laporan transaction Istana Variasi </div>
                                                    </div>
                                                </div><!-- .invoice-bills -->
                                            </div><!-- .invoice-wrap -->
                                        </div><!-- .invoice -->
                                    </div><!-- .nk-block -->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
        <!-- app-root @e -->
        <!-- JavaScript -->
        <script src="./assets/js/bundle.js?ver=2.2.0"></script>
        <script src="./assets/js/scripts.js?ver=2.2.0"></script>
    </body>
</html>