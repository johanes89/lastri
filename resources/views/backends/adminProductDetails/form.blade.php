{{ Form::hidden(
    'product_id',
    $menu,
) }}
<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Warna</label>
            {{ Form::text(
                'color',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Merah, Biru, Ungu, Hitam, Putih, dll',
                    )
            ) }}
        </div>
        @if ($errors->has('color'))
            <span class="form-note text-danger">{{ $errors->first('color') }}</span>
        @endif
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Variasi</label>
            {{ Form::text(
                'variation',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => 'Merah Biru, Hitam Benang Putih',
                    )
            ) }}
        </div>
        @if ($errors->has('variation'))
            <span class="form-note text-danger">{{ $errors->first('variation') }}</span>
        @endif
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="form-label" for="default-1-01">Jumlah</label>
            {{ Form::number(
                'qty',
                null,
                array(
                    'class' => 'form-control',
                    'id' => 'default-1-01',
                    'placeholder' => '10',
                    )
            ) }}
        </div>
        @if ($errors->has('qty'))
            <span class="form-note text-danger">{{ $errors->first('qty') }}</span>
        @endif
    </div>
</div>
<div class="row gy-4">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="form-control-wrap">
                <label class="form-label">Gambar</label>
                <div class="mb-1">
                    {{ (isset($dataReference)?$dataReference->fileImageUrl:'') }}
                </div>
                <div class="custom-file">
                    <input type="file" name="photo_url" multiple class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">jpg, png</label>
                </div>
                @if ($errors->has('photo_url'))
                    <span class="form-note text-danger">{{ $errors->first('photo_url') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>