<div class="card-inner card-inner-lg">
    <h4 class="nk-block-title">Pengaturan Keamanan</h4>
    <hr class="preview-hr mt-2 mb-2">
    <div class="nk-block">
        <div class="card">
            <div class="card-inner-group">
                <div class="card-inner">
                {{ Form::model(
                    $dataReference,
                    [
                        'method' => $routeMethod,
                        'route' => [
                            $route,
                            $dataReferenceId
                        ],
                        'files' => $fileUpload
                    ]
                ) }}
                <div class="row gy-4">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="default-1-01">Nama</label>
                            {{ Form::text(
                                'name',
                                null,
                                array(
                                    'class' => 'form-control',
                                    'id' => 'default-1-01',
                                    'placeholder' => 'Name',
                                    'readonly' => true
                                    )
                            ) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="default-1-02">Email</label>
                            {{ Form::email(
                                'email',
                                null,
                                array(
                                    'class' => 'form-control',
                                    'id' => 'default-1-02',
                                    'placeholder' => 'Email',
                                    'readonly' => true
                                    )
                            ) }}
                        </div>
                    </div>
                </div>
                <div class="row gy-4">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="password">Password Baru</label>
                            <div class="form-control-wrap">
                                <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                    <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                    <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                </a>
                                <input type="password" name="password" class="form-control form-control-lg" id="password" placeholder="Enter your new password">
                            </div>
                            @if ($errors->has('password'))
                                <span class="form-note text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="password">Konfirmasi Password Baru</label>
                            <div class="form-control-wrap">
                                <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                    <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                    <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                </a>
                                <input type="password" name="password_confirmation" class="form-control form-control-lg" id="password" placeholder="Enter password confirmation">
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="form-note text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                {!! Form::submit(
                    'Update',
                    ['class'=>'form-group mt-2 btn btn-primary']
                ) !!}
                
                {!! Form::close() !!}
                </div><!-- .card-inner -->

            </div><!-- .card-inner-group -->
        </div><!-- .card -->
    </div><!-- .nk-block -->
</div><!-- .card-inner -->