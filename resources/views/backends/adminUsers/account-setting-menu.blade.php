<div class="card-inner p-0">
    <ul class="link-list-menu">
        <li><a class="{{ ($menuId == 'personal-setting')?'active':'' }}" href="{{ route('admin-users.personalSetting') }}"><em class="icon ni ni-user-fill-c"></em><span>Informasi Pribadi</span></a></li>
        <!-- <li><a href="html/user-profile-notification.html"><em class="icon ni ni-bell-fill"></em><span>Notifications</span></a></li>
        <li><a href="html/user-profile-activity.html"><em class="icon ni ni-activity-round-fill"></em><span>Account Activity</span></a></li> -->
        <li><a class="{{ ($menuId == 'security-setting')?'active':'' }}" href="{{ route('admin-users.securitySetting') }}"><em class="icon ni ni-lock-alt-fill"></em><span>Pengaturan Keamanan</span></a></li>
    </ul>
</div>