<!-- @@ Profile Edit Modal @e -->
<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-lg">
                <h5 class="title">Perbarui Informasi Pribadi</h5>
                <hr class="preview-hr mt-2 mb-2">
                {{ Form::model(
                    $dataReference,
                    [
                        'method' => $routeMethod,
                        'route' => [
                            $route,
                            $dataReferenceId
                        ],
                        'files' => $fileUpload
                    ]
                ) }}
                <div class="row gy-4">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="default-1-01">Nama</label>
                            {{ Form::text(
                                'name',
                                null,
                                array(
                                    'class' => 'form-control',
                                    'id' => 'default-1-01',
                                    'placeholder' => 'Name',
                                    'required' => true
                                    )
                            ) }}
                        </div>
                        @if ($errors->has('name'))
                            <span class="form-note text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="form-label" for="default-1-02">Email</label>
                            {{ Form::email(
                                'email',
                                null,
                                array(
                                    'class' => 'form-control',
                                    'id' => 'default-1-02',
                                    'placeholder' => 'Email',
                                    'required' => true
                                    )
                            ) }}
                        </div>
                        @if ($errors->has('email'))
                            <span class="form-note text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row gy-4">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <div class="custom-file">
                                    <input type="file" name="profile_photo_url" multiple class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                                @if ($errors->has('profile_photo_url'))
                                    <span class="form-note text-danger">{{ $errors->first('profile_photo_url') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::submit(
                    'Update',
                    ['class'=>'form-group mt-2 btn btn-primary']
                ) !!}
                
                {!! Form::close() !!}
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- .modal -->