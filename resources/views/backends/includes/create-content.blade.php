<div class="nk-block nk-block-lg">
    {{-- <div class="nk-block-head">
        <div class="nk-block-head-content">
            @if(isset($message))
            <div class="nk-block-des">
                <p>{{ (isset($message)?$message:'') }}</p>
            </div>
            @endif
        </div>
    </div> --}}
    <div class="card card-preview">
        <div class="card-inner">
            <div class="preview-block">
            <span class="preview-title-lg overline-title">{{ (isset($label)?$label:'No Label Set') }}</span>
            <hr class="preview-hr mt-2 mb-2">
            {!! Form::open([
                'route' => [$route, $menu], 
                'method' => $routeMethod,
                'files' => $fileUpload
            ]) !!}

            @include($form)

            {!! Form::submit(
                'Create',
                ['class'=>'form-group mt-2 btn btn-primary']
            ) !!}

            {!! Form::close() !!}
            </div>
        </div><!-- card -->
    </div><!-- .nk-block -->
</div>