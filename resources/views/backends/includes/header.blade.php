<div class="nk-header nk-header-fixed is-custom">
    <div class="container-fluid">
        <div class="nk-header-wrap">
            <div class="nk-menu-trigger d-xl-none ml-n1">
                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
            </div>
            <div class="nk-header-brand d-xl-none">
                
            @include('backends.includes.logo-header-mobile')
            
            </div><!-- .nk-header-brand -->
            <!-- .nk-header-news -->
            <div class="nk-header-tools">
                <ul class="nk-quick-nav">
                    {{-- @include('backends.includes.message-dropdown-header') --}}
                    {{-- @include('backends.includes.notification-dropdown-header') --}}
                    @include('backends.includes.account-dropdown-header')     
                </ul>
            </div>
        </div><!-- .nk-header-wrap -->
    </div><!-- .container-fliud -->
</div>