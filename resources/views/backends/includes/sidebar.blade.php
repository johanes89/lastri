<div class="nk-sidebar nk-sidebar-fixed is-light" data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            @include('backends.includes.logo-sidebar')
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em>
            </a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">

                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Panel Menu</h6>
                    </li><!-- .nk-menu-item -->
                    @foreach($configurationMenus as $menu)
                        <li class="nk-menu-item">
                            <a href="{{ $menu->url }}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="{{ $menu->icon }}"></em></span>
                                <span class="nk-menu-text">{{ $menu->label }}</span>
                            </a>
                        </li><!-- .nk-menu-item -->
                    @endforeach                    

                    @if(auth()->user()->isAdministrator)
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Transaksi</h6>
                    </li><!-- .nk-menu-heading -->
                    @endif
                    @foreach($transactionMenus as $menu)
                        <li class="nk-menu-item">
                            <a href="{{ $menu->url }}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="{{ $menu->icon }}"></em></span>
                                <span class="nk-menu-text">{{ $menu->label }}</span>
                            </a>
                        </li><!-- .nk-menu-item -->
                    @endforeach     
                    
                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>