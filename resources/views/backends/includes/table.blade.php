<div class="nk-block nk-block-lg">
    <div class="card">
        <div class="card-inner">
            <div class="card-head">
                <h5 class="card-title">{{ (isset($labelTable)?$labelTable:'No Label Table Set') }}</h5>
            </div>
            <div class="table-responsive">
                    <table style="width:100%" class="table table-bordered w-100 display compact datatable"
                    data-source="{!! $routeDatatable !!}"
                    data-columns="{{ json_encode($datatableColumns->dataColumns) }}">
                        <thead>
                            <tr>
                                @foreach($datatableColumns->labelColumns as $row)
                                    <th>{{ $row }}</th>
                                @endforeach
                            </tr>
                        </thead>
                    </table>
                </div>
        </div>
    </div><!-- card -->
</div><!-- .nk-block -->