<a href="{{ url(!empty($configuration->dashboard_route)?$configuration->dashboard_route:'#') }}" class="logo-link nk-sidebar-logo">
    <img class="logo-light logo-img" src="{{ asset($configuration->logoUrl) }}" srcset="{{ asset($configuration->smallLogoUrl) }} 2x" alt="logo">
    <img class="logo-dark logo-img" src="{{ asset($configuration->logoUrl) }}" srcset="{{ asset($configuration->smallLogoUrl) }} 2x" alt="logo-dark">
    <img class="logo-small logo-img logo-img-small" src="{{ asset($configuration->logoUrl) }}" srcset="{{ asset($configuration->smallLogoUrl) }} 2x" alt="logo-small">
</a>