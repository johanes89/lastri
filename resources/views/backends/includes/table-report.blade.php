<div class="nk-block nk-block-lg">
    <div class="card">
        <div class="card-inner">
            <div class="card-head">
                <h5 class="card-title">{{ (isset($labelTable)?$labelTable:'No Label Table Set') }}</h5>
                <p>
                    <form method="POST" action="{{ route('admin-reports.print') }}">
                            @csrf
                            <input type="date" name="date1" class="btn btn-success" required>
                            s/d <input type="date" name="date2" class="btn btn-primary" required>
                            <button type="submit" href="{{ route('admin-reports.print') }}" class="btn btn-warning">Print</button>
                    </form>
                </p>
            </div>
            <div class="table-responsive">
                    <table style="width:100%" class="table table-bordered w-100 display compact datatable"
                    data-source="{!! $routeDatatable !!}"
                    data-columns="{{ json_encode($datatableColumns->dataColumns) }}">
                        <thead>
                            <tr>
                                @foreach($datatableColumns->labelColumns as $row)
                                    <th>{{ $row }}</th>
                                @endforeach
                            </tr>
                        </thead>
                    </table>
                </div>
        </div>
    </div><!-- card -->
</div><!-- .nk-block -->