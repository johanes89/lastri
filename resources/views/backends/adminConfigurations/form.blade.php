<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label" for="site-name">Nama</label>
            <span class="form-note">Nama Aplikasi kamu</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'name',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Application Name',
                        'required' => true
                        )
                ) }}
            </div>
            @if ($errors->has('name'))
                <span class="form-note text-danger">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Logo</label>
            <span class="form-note">Unggah logo perusahaan kamu</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                <div class="custom-file">
                    <input type="file" name="logo" multiple class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Pilih File Gambar</label>
                </div>
                @if ($errors->has('logo'))
                    <span class="form-note text-danger">{{ $errors->first('logo') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Logo Kecil</label>
            <span class="form-note">Unggah logo kecil perusahaan kamu</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                <div class="custom-file">
                    <input type="file" name="small_logo" multiple class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Pilih File Gambar</label>
                </div>
                @if ($errors->has('small_logo'))
                    <span class="form-note text-danger">{{ $errors->first('small_logo') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Ikon</label>
            <span class="form-note">Unggah gambar ikon</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                <div class="custom-file">
                    <input type="file" name="icon" multiple class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Pilih File Gambar</label>
                </div>
                @if ($errors->has('icon'))
                    <span class="form-note text-danger">{{ $errors->first('icon') }}</span>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Informasi Pembayaran</label>
            <span class="form-note">Informasi Pembayaran ini akan ditampilkan di bagian pelanggan</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::textarea(
                    'payment_information',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Description',
                        'rows' => 3
                        )
                ) }}
            </div>
        </div>
    </div>
</div>

<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Kata Kunci</label>
            <span class="form-note">Kata kunci akan di pasangkan di meta header</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'keywords',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Keywords',
                        )
                ) }}
            </div>
            @if ($errors->has('keywords'))
                <span class="form-note text-danger">{{ $errors->first('keywords') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Email perusahaan</label>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'official_email',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Official Email',
                        )
                ) }}
            </div>
            @if ($errors->has('official_email'))
                <span class="form-note text-danger">{{ $errors->first('official_email') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Telepon perusahaan</label>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'official_phone',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Official Phone',
                        )
                ) }}
            </div>
            @if ($errors->has('official_phone'))
                <span class="form-note text-danger">{{ $errors->first('official_phone') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Alamat perusahaan</label>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'official_address',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Official Address',
                        )
                ) }}
            </div>
            @if ($errors->has('official_address'))
                <span class="form-note text-danger">{{ $errors->first('official_address') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Copyright Label</label>
            <span class="form-note">Jika kosong akan di tampilkan powered by</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'copyright_label',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Copyright',
                        )
                ) }}
            </div>
            @if ($errors->has('copyright_label'))
                <span class="form-note text-danger">{{ $errors->first('copyright_label') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Copyright</label>
            <span class="form-note">Jika kosong akan ditampilkan created by</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'copyright',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Copyright',
                        )
                ) }}
            </div>
            @if ($errors->has('copyright'))
                <span class="form-note text-danger">{{ $errors->first('copyright') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Copyright Url</label>
            <span class="form-note">Berisi url perusahan</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'copyright_url',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Copyright Url',
                        )
                ) }}
            </div>
            @if ($errors->has('copyright_url'))
                <span class="form-note text-danger">{{ $errors->first('copyright_url') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Tahun</label>
            <span class="form-note">Tahun pembuatan aplikasi</span>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::number(
                    'year',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => '2020',
                        )
                ) }}
            </div>
            @if ($errors->has('year'))
                <span class="form-note text-danger">{{ $errors->first('year') }}</span>
            @endif
        </div>
    </div>
</div>
<div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label">Versi</label>
            <span class="form-note">Versi dari aplikasi</span>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="form-control-wrap">
                {{ Form::text(
                    'version',
                    null,
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Version 1.0',
                        )
                ) }}
            </div>
            @if ($errors->has('version'))
                <span class="form-note text-danger">{{ $errors->first('version') }}</span>
            @endif
        </div>
    </div>
</div>

<!-- <div class="row g-3 align-center">
    <div class="col-lg-5">
        <div class="form-group">
            <label class="form-label" for="site-off">Maintanance Mode</label>
            <span class="form-note">Enable to make website make offline.</span>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" name="reg-public" id="site-off">
                <label class="custom-control-label" for="site-off">Offline</label>
            </div>
        </div>
    </div>
</div> -->