<div class="nk-block">
    <div class="row g-gs">
        <div class="col-xxl-6">
            <div class="nk-block-head nk-block-head-lg wide-sm">
                <div class="nk-block-head-content">
                    <div class="nk-block-head-sub"><a class="back-to" href="html/components.html"><em class="icon ni ni-layers"></em><span>Dashboard</span></a></div>
                    <h4 class="nk-block-title fw-normal">Selamat datang kembali, {{ auth()->user()->name }}</h4>
                    <div class="nk-block-des">
                        <p class="lead">Saat ini kamu login sebagai {{ auth()->user()->userRole->role->name }}</p>
                    </div>
                </div>
            </div>
        </div><!-- .col -->
    </div><!-- .row -->
</div><!-- .nk-block -->