<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(resource_path('json/countries.json'));
        $data = json_decode($json, true);

        foreach ($data as $obj) {
            $existing = Country::find($obj['id']);

            if (empty($existing->id)) {
                $existing = new Country;
            }

            $existing->fill($obj);
            $existing->save();
        }
    }
}
