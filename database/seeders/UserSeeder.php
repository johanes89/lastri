<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\AdminUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(resource_path('json/users.json'));
        $data = json_decode($json, true);

        foreach ($data as $obj) {
            $existing = AdminUser::find($obj['id']);

            if (empty($existing->id)) {
                $existing = new AdminUser;
            }

            $existing->fill($obj);
            
            $existing->save();
            $existing->roles()->sync(1);
        }

    }
}
