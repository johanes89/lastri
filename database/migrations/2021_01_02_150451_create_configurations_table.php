<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->text('logo')->nullable();
            $table->text('small_logo')->nullable();
            $table->text('icon')->nullable();
            $table->text('payment_information')->nullable();
            $table->text('keywords')->nullable();
            $table->string('official_email')->nullable();
            $table->string('official_phone')->nullable();
            $table->string('official_address')->nullable();
            $table->string('copyright_label')->nullable();
            $table->string('copyright')->nullable();
            $table->string('copyright_url')->nullable();
            $table->year('year')->nullable();
            $table->string('version')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
