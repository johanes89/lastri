<?php

namespace App\Policies;

use App\Models\AdminUser;
use App\Models\GroupContent;
use App\Models\UserMenuPrivilege;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupContentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function allowAction(AdminUser $user)
    {
        $allowed = false;

        if (
            $user->isOwner ||
            $user->isAdministrator
        ) {
            $allowed = true;
        }

        return $allowed;
    }

}
