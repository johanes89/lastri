<?php

namespace App\Exporter;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use App\Models\Transaction;
use Carbon\Carbon;

class ExportLaporan implements FromQuery, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $date1;
    private $date2;

    public function __construct($date1, $date2)
    {
        $this->date1 = Carbon::parse($date1)->copy()->startOfDay();
        $this->date2 = Carbon::parse($date2)->copy()->endOfDay();
    }

    public function query()
    {
        $query = Transaction::query()
        ->with([
            'transactionDetail',
            'country',
            'province',
            'regency',
            'district',
            'subDistrict',
            'shippingMethod',
            'paymentMethod'
        ])
        ->whereBetween('created_at', [$this->date1, $this->date2]);
        return $query;
    }

    public function map($transaction): array
    {
        $gender = 'Laki-Laki';
        if(isset($transaction->gender)){
            if($transaction->gender == 'P'){
                $gender = 'Perempuan';
            }
        }

        $productDetail = $transaction->transactionDetail->map(function($product, $key){
            return ($key+1).". ".
            $product->productDetail->productDetailName." - ".
            "jumlah: ".$product->qty." - ".
            "Total: ".$product->totalFormat;
        });

        return [
            'transaction_code' => (isset($transaction->transaction_code)?$transaction->transaction_code:'-'),
            'fullname' => (isset($transaction->fullname)?$transaction->fullname:'-'),
            'gender' => $gender,
            'phone' => (isset($transaction->phone)?$transaction->phone:'-'),
            'email' => (isset($transaction->email)?$transaction->email:'-'),
            'country' => (isset($transaction->country_id)?$transaction->country->name:'-'),
            'province' => (isset($transaction->province_id)?$transaction->province->name:'-'),
            'regency' => (isset($transaction->regency_id)?$transaction->regency->name:'-'),
            'district' => (isset($transaction->district_id)?$transaction->district->name:'-'),
            'sub_district' => (isset($transaction->sub_district_id)?$transaction->subDistrict->name:'-'),
            'address' => (isset($transaction->address)?$transaction->address:'-'),
            'postalcode' => (isset($transaction->postalcode)?$transaction->postalcode:'-'),
            'shipping_method' => (isset($transaction->shipping_method_id)?$transaction->shippingMethod->name:'-'),
            'expedition' => (isset($transaction->expedition)?$transaction->expedition:'-'),
            'shipping_cost' => (isset($transaction->shipping_cost)?$transaction->shipping_cost:'0'),
            'receipt_code' => (isset($transaction->receipt_code)?$transaction->receipt_code:'-'),
            'grand_total' => (isset($transaction->grand_total)?$transaction->grand_total:'0'),
            'payment_method' => (isset($transaction->payment_method_id)?$transaction->paymentMethod->name:'-'),
            'product_detail' => $productDetail,
            'status' => (isset($transaction->status)?$transaction->displayStatus:'-'),
            'created_at' => (isset($transaction->created_at)?Carbon::parse($transaction->created_at)->format('d-m-Y H:i:s'):'-'),
        ];
    }

    public function headings(): array
    {
        return [
            'transaction_code' => 'Kode Transaksi',
            'fullname' => 'Nama Lengkap',
            'gender' => 'Jenis Kelamin',
            'phone' => 'Telepon',
            'email' => 'Email',
            'country' => 'Negara',
            'province' => 'Provinsi',
            'regency' => 'Kota',
            'district' => 'Kecamatan',
            'sub_district' => 'Kelurahan',
            'address' => 'Alamat',
            'postalcode' => 'Kode Pos',
            'shipping_method' => 'Metode Pengiriman',
            'expedition' => 'Ekspedisi',
            'shipping_cost' => 'Biaya Kirim',
            'receipt_code' => 'No Resi',
            'grand_total' => 'Total',
            'payment_method' => 'Metode Pembayaran',
            'product_detail' => 'Daftar Produk',
            'status' => 'Status',
            'created_at' => 'Tanggal Transaksi',
        ];
    }
}
