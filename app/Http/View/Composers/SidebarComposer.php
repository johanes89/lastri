<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Models\Configuration;
use App\Models\GroupMenu;
use App\Helpers\_function;

class SidebarComposer
{
    private $user;

    /**
     * The user repository implementation.
     *
     * @var \App\Repositories\UserRepository
     */

    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->user = auth()->user();
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(
            'transactionMenus', 
            $this->getTransactionMenus()
        );

        $view->with(
            'configuration', 
            $this->getMenus()
        );

        $view->with(
            'configurationMenus', 
            $this->getConfigurationMenus()
        );
        
    }

    public function getTransactionMenus()
    {
        $transactionMenus = collect();

        if(
            $this->user->isAdministrator
            ){

            $transactionMenus->push((object)[
                'label' => 'Slideshow',
                'url' => route('admin-slideshow.index'),
                'icon' => 'icon ni ni-tile-thumb'
            ]);

            $transactionMenus->push((object)[
                'label' => 'Kategori',
                'url' => route('admin-ref-categories.index'),
                'icon' => 'icon ni ni-dot-box'
            ]);

            $transactionMenus->push((object)[
                'label' => 'Metode Pengiriman',
                'url' => route('admin-ref-shipping-methods.index'),
                'icon' => 'icon ni ni-speed'
            ]);

            $transactionMenus->push((object)[
                'label' => 'Produk',
                'url' => route('admin-products.index'),
                'icon' => 'icon ni ni-invest'
            ]);

            $transactionMenus->push((object)[
                'label' => 'Transaksi',
                'url' => route('admin-transactions.index'),
                'icon' => 'icon ni ni-tranx'
            ]);
    
        }

        return $transactionMenus;
    }

    public function getMenus()
    {
        $configuration = Configuration::get()->first();
        // dd($configuration);

        return $configuration;
    }

    public function getConfigurationMenus()
    {
        $configurationMenus = collect();

        if($this->user->isOwner){
            $configurationMenus->push((object)[
                'label' => 'Konfigurasi',
                'url' => route('admin-configurations.index'),
                'icon' => 'icon ni ni-dashboard'
            ]);

            $configurationMenus->push((object)[
                'label' => 'User',
                'url' => route('admin-users.index'),
                'icon' => 'icon ni ni-users'
            ]);

            $configurationMenus->push((object)[
                'label' => 'Laporan',
                'url' => route('admin-reports.index'),
                'icon' => 'icon ni ni-tile-thumb'
            ]);
    
        }

        return $configurationMenus;
    }

}