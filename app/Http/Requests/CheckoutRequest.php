<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'fullname' => [
                'required',
                'string',
            ],
            'gender' => [
                'required',
            ],
            'phone' => [
                'required',
            ],
            'email' => [
                'required',
                'email'
            ],
            'province_id' => [
                'required',
            ],
            'regency_id' => [
                'required',
            ],
            'district_id' => [
                'required',
            ],
            'sub_district_id' => [
                'required',
            ],
            'address' => [
                'required',
            ],
            'shipping_method_id' => [
                'required',
            ],
        ];
    }
}
