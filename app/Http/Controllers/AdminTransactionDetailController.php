<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransactionDetail;
use Yajra\Datatables\Datatables;
use DB;

class AdminTransactionDetailController extends Controller
{
    public function datatable($transaction_id)
    {
        $query = TransactionDetail::query();
        $query->with([
            'productDetail' => function ($q){
                $q->with([
                    'product'
                ]);
            },
        ]);

            return Datatables::of($query)
                ->filterColumn('product_name', function ($q, $keyword) {
                    $q->whereHas('productDetail', function ($q) use ($keyword) {
                        $q->whereHas('product', function ($q) use ($keyword) {
                            $q->whereRaw(
                                "concat(products.name) like ?",
                                ["%{$keyword}%"]
                            );
                        });
                    });
                })

                ->filterColumn('product_detail', function ($q, $keyword) {
                    $q->whereHas('productDetail', function ($q) use ($keyword) {
                        $q->whereRaw(
                            "concat(product_details.color) like ?",
                            ["%{$keyword}%"]
                        );
                    });
                })

                ->editColumn('product_detail_id', function($adminTransactionDetail){
                    return $adminTransactionDetail->productDetail->fileImageThumb ?? null;
                })
                ->editColumn('product_name', function($adminTransactionDetail){
                    return $adminTransactionDetail->productDetail->product->name ?? null;
                })
                ->editColumn('product_detail', function($adminTransactionDetail){
                    return $adminTransactionDetail->productDetail->productDetailName ?? null;
                })
                ->editColumn('qty', function($adminTransactionDetail){
                    return $adminTransactionDetail->qty." item" ?? null;
                })
                ->editColumn('total', function($adminTransactionDetail){
                    return $adminTransactionDetail->totalFormat ?? null;
                })
                ->make(true);
    }
}