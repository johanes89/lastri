<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $notAllowed = RestrictedUser::notAllowedUser();
        
        if($notAllowed){
            $request->session()->flush();
            redirect('login');
        }else{
            return view('backends.home.index');
        }
        
    }
}
