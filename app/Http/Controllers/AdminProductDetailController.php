<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductDetail;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\AdminProductDetailRequest;
use App\Helpers\Media;

class AdminProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($product_id)
    {
        $form = 'backends.adminProductDetails.form';
        $menu = $product_id;
        $dataReference = Product::find($product_id);

        $action = 'backends.includes.create-content';
        $route = 'admin-product-details.store';
        $routeMethod = 'POST';

        $label = 'Tambah Variasi Produk';

        $labelTable = 'Daftar Variasi Produk '.$dataReference->name;
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-product-details.datatable', ['product_id' => $product_id]);
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = true;

        return view('backends.adminProductDetails.index', 
        compact(
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
            'menu'
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'color'],
                [ 'data' => 'variation'],
                [ 'data' => 'qty'],
                [ 'data' => 'photo_url'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Warna',
                'Variasi',
                'Kuantitas',
                'Foto',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminProductDetailRequest $request)
    {
        try {
            DB::beginTransaction();
            $adminProductDetail = new ProductDetail;
            $adminProductDetail = $adminProductDetail->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-product-details.index', ['product_id' => $request->product_id])
            ->with('success_message', 'Detil Produk berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = ProductDetail::find($id);
        $dataReferenceId = $dataReference->id;
        $menu = $dataReference->product_id;

        $form = 'backends.adminProductDetails.form';
        $action = 'backends.includes.edit';
        $route = 'admin-product-details.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Detil Produk';
        $labelTable = 'Daftar Produk';
        
        $fileUpload = true;

        return view('backends.adminProductDetails.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
            'menu'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminProductDetailRequest $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminProductDetail = ProductDetail::find($id);
            $old_path_image = $adminProductDetail->photo_url;

            $adminProductDetail = $adminProductDetail->saveFromRequest($request);

            if ($adminProductDetail->photo_url != $old_path_image) {

                if (Media::fileExists($old_path_image)) {

                    Media::deleteFileUpload($old_path_image);
    
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-product-details.index', ['product_id' => $adminProductDetail->product_id])
            ->with('success_message', 'Detil Produk berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminProductDetail = ProductDetail::find($id);
            $old_path_image = $adminProductDetail->photo_url;
            $adminProductDetail->delete();

                if (Media::fileExists($old_path_image)) {

                    Media::deleteFileUpload($old_path_image);
    
                }
                
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Detil Produk berhasil dihapus!');
        return ['success'];
    }

    public function datatable($product_id)
    {
        $query = ProductDetail::query()
        ->where('product_id', '=', $product_id);
        $query->with([
            'product'
        ]);

            return Datatables::of($query)
                ->addColumn('action', function ($adminProductDetail) use($product_id){
                    $editLink = "<a href=".route('admin-product-details.edit', ['id' => $adminProductDetail->id])." class='btn btn-primary btn-wth-Product Product-wthot-bg btn-sm mb-1'><span class='Product-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>ubah</span></a>";

                    $deleteLink = "<a href=".route('admin-product-details.destroy', ['id' => $adminProductDetail->id])." class='btn btn-danger btn-wth-Product Product-wthot-bg btn-sm delete-button'><span class='Product-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>hapus</span></a>";
                    return $editLink . $deleteLink;
                })
                ->editColumn('photo_url', function($adminProductDetail){
                    return $adminProductDetail->fileImageThumb ?? null;
                })
                ->rawColumns(['action'])
                ->toJson();
    }
}