<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminConfiguration;
use App\Models\GroupMenu;
use App\Models\Role;
use App\Models\Configuration;
use App\Models\ConfigurationMenuPrivilege;
use Yajra\Datatables\Datatables;
use App\Http\Requests\AdminConfigurationRequest;
use DB;
use Illuminate\Support\Arr;
use App\Helpers\Media;
use App\Helpers\RestrictedUser;
use Auth;

class AdminConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labelTable = 'Daftar Konfigurasi';
        $table = 'backends.includes.table';

        $fileUpload = true;

        $routeDatatable = route('admin-configurations.datatable');
        $datatableColumns = $this->getDatatableColumns();

        return view('backends.adminConfigurations.index', 
        compact(
            'table',
            'labelTable',
            'fileUpload',
            'datatableColumns',
            'routeDatatable'
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'logo'],
                [ 'data' => 'small_logo'],
                [ 'data' => 'icon'],
                [ 'data' => 'payment_information'],
                [ 'data' => 'keywords'],
                [ 'data' => 'official_email'],
                [ 'data' => 'official_phone'],
                [ 'data' => 'official_address'],
                [ 'data' => 'copyright_label'],
                [ 'data' => 'copyright'],
                [ 'data' => 'copyright_url'],
                [ 'data' => 'year'],
                [ 'data' => 'version'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Logo',
                'Logo Kecil',
                'Ikon',
                'Informasi Pembayaran',
                'Kata Kunci',
                'Email',
                'Telepon',
                'Alamat',
                'Label Copyright',
                'Copyright',
                'Url Copyright',
                'Tahun',
                'Versi',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Configuration::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminConfigurations.form';

        $action = 'backends.includes.edit';
        $route = 'admin-configurations.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Konfigurasi';
        $labelTable = 'Daftar Konfigurasi';
        
        $fileUpload = true;

        return view('backends.adminConfigurations.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminConfigurationRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $adminConfiguration = Configuration::find($id);
            $old_path_logo = $adminConfiguration->logo;
            $old_path_small_logo = $adminConfiguration->small_logo;
            $old_path_icon = $adminConfiguration->icon;
            
            $adminConfiguration = $adminConfiguration->saveFromRequest($request);

            if ($adminConfiguration->logo != $old_path_logo) {

                if (Media::fileExists($old_path_logo)) {

                    Media::deleteFileUpload($old_path_logo);
    
                }
            }

            if ($adminConfiguration->small_logo != $old_path_small_logo) {

                if (Media::fileExists($old_path_small_logo)) {

                    Media::deleteFileUpload($old_path_small_logo);
    
                }
            }

            if ($adminConfiguration->icon != $old_path_icon) {

                if (Media::fileExists($old_path_icon)) {

                    Media::deleteFileUpload($old_path_icon);
    
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-configurations.index')
            ->with('success_message', 'Konfigurasi berhasil diubah!');
    }

    public function datatable()
    {
        $query = Configuration::query();

        if (request()->ajax()) {
            return Datatables::of($query)
                ->addColumn('action', function ($adminConfiguration) {
                    $editLink = "<a href=".route('admin-configurations.edit', ['id' => $adminConfiguration->id])." class='btn btn-primary btn-wth-icon icon-wthot-bg btn-sm mb-1'><span class='icon-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>ubah</span></a>";

                    return $editLink;
                })

                ->editColumn('logo', function($adminConfiguration){
                    return $adminConfiguration->logoThumb ?? null;
                })

                ->editColumn('small_logo', function($adminConfiguration){
                    return $adminConfiguration->smallLogoThumb ?? null;
                })

                ->editColumn('icon', function($adminConfiguration){
                    return $adminConfiguration->iconThumb ?? null;
                })

                ->rawColumns(['action'])
                ->toJson();
        }
    }
}
