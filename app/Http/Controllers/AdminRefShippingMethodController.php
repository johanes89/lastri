<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShippingMethod;
use Yajra\Datatables\Datatables;
use DB;

class AdminRefShippingMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = 'backends.adminRefShippingMethods.form';

        $action = 'backends.includes.create';
        $route = 'admin-ref-shipping-methods.store';
        $routeMethod = 'POST';

        $label = 'Tambah Metode Pengiriman';

        $labelTable = 'Daftar Metode Pengiriman';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-ref-shipping-methods.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        return view('backends.adminRefShippingMethods.index', 
        compact(
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $adminRefShippingMethod = new ShippingMethod;
            $adminRefShippingMethod = $adminRefShippingMethod->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-ref-shipping-methods.index')
            ->with('success_message', 'Metode Pengiriman berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = ShippingMethod::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminRefShippingMethods.form';
        $action = 'backends.includes.edit';
        $route = 'admin-ref-shipping-methods.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Metode Pengiriman';
        $labelTable = 'Daftar Metode Pengiriman';
        
        $fileUpload = false;

        return view('backends.adminRefShippingMethods.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminRefShippingMethod = ShippingMethod::find($id);
            $adminRefShippingMethod = $adminRefShippingMethod->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-ref-shipping-methods.index')
            ->with('success_message', 'Metode Pengiriman berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminRefShippingMethod = ShippingMethod::find($id);
            $adminRefShippingMethod->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Metode Pengiriman berhasil dihapus!');
        return ['success'];
    }

    public function datatable()
    {
        $query = ShippingMethod::query();

            return Datatables::of($query)
                ->filterColumn('name', function ($q, $keyword) {
                    $q->whereRaw(
                        "name like ?",
                        ["%{$keyword}%"]
                    );
                })
                ->addColumn('action', function ($adminRefShippingMethod) {
                    $editLink = "<a href=".route('admin-ref-shipping-methods.edit', ['admin_ref_shipping_method' => $adminRefShippingMethod->id])." class='btn btn-primary btn-wth-ShippingMethod ShippingMethod-wthot-bg btn-sm mb-1'><span class='ShippingMethod-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>ubah</span></a>";

                    $deleteLink = "<a href=".route('admin-ref-shipping-methods.destroy', ['admin_ref_shipping_method' => $adminRefShippingMethod->id])." class='btn btn-danger btn-wth-ShippingMethod ShippingMethod-wthot-bg btn-sm delete-button'><span class='ShippingMethod-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>hapus</span></a>";
                    return $editLink . $deleteLink;
                })
                ->rawColumns(['action'])
                ->make(true);
    }
}