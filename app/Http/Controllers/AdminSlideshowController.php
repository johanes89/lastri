<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slideshow;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\AdminSlideshowRequest;
use App\Helpers\Media;

class AdminSlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form = 'backends.adminSlideshow.form';

        $action = 'backends.includes.create';
        $route = 'admin-slideshow.store';
        $routeMethod = 'POST';

        $label = 'Tambah Slideshow';

        $labelTable = 'Daftar Slidershow';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-slideshow.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = true;

        return view('backends.adminSlideshow.index', 
        compact(
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'photo_url'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Foto',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminSlideshowRequest $request)
    {
        try {
            DB::beginTransaction();
            $adminSlideshow = new Slideshow;
            $adminSlideshow = $adminSlideshow->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-slideshow.index')
            ->with('success_message', 'Slideshow berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Slideshow::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminSlideshow.form';
        $action = 'backends.includes.edit';
        $route = 'admin-slideshow.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Slideshow';
        $labelTable = 'Daftar Slideshow';
        
        $fileUpload = true;

        return view('backends.adminSlideshow.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminSlideshowRequest $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminSlideshow = Slideshow::find($id);
            $old_path_image = $adminSlideshow->photo_url;

            $adminSlideshow = $adminSlideshow->saveFromRequest($request);

            if ($adminSlideshow->photo_url != $old_path_image) {

                if (Media::fileExists($old_path_image)) {

                    Media::deleteFileUpload($old_path_image);
    
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-slideshow.index')
            ->with('success_message', 'Slideshow berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $adminSlideshow = Slideshow::find($id);
            $old_path_image = $adminSlideshow->photo_url;

            $adminSlideshow->delete();

            if ($adminSlideshow->photo_url != $old_path_image) {

                if (Media::fileExists($old_path_image)) {

                    Media::deleteFileUpload($old_path_image);
    
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Slideshow berhasil dihapus');
        return ['success'];
    }

    public function datatable()
    {
        $query = Slideshow::query();

            return Datatables::of($query)

            ->addColumn('action', function ($adminSlideshow) {
                $editLink = "<a href=".route('admin-slideshow.edit', ['admin_slideshow' => $adminSlideshow->id])." class='btn btn-primary btn-wth-Slideshow Slideshow-wthot-bg btn-sm mb-1'><span class='Slideshow-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>edit</span></a>";

                $deleteLink = "<a href=".route('admin-slideshow.destroy', ['admin_slideshow' => $adminSlideshow->id])." class='btn btn-danger btn-wth-Slideshow Slideshow-wthot-bg btn-sm delete-button'><span class='Slideshow-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>delete</span></a>";
                return $editLink . $deleteLink;
            })
            ->editColumn('photo_url', function($adminSlideshow){
                return $adminSlideshow->fileImageThumb ?? null;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}