<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Models\Role;
use Yajra\Datatables\Datatables;
use App\Http\Requests\adminRoleRequest;
use DB;

class AdminRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labelTable = 'Daftar Rule';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-roles.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        return view('backends.adminRoles.index', 
        compact(
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'name'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $adminRole = new Role;
            $adminRole = $adminRole->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-roles.index')
            ->with('success_message', 'Nama Rule berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Role::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminRoles.form';
        $action = 'backends.includes.edit';
        $route = 'admin-roles.update';
        $routeMethod = 'PATCH';

        $label = 'Ubah Rule';
        $message = 'Menu ini digunakan untuk mengubah nama role';
        $labelTable = 'Daftar Rule';
        
        $fileUpload = false;

        return view('backends.adminRoles.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'message',
            'labelTable',
            'fileUpload',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminRole = Role::find($id);
            $adminRole = $adminRole->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-roles.index')
            ->with('success_message', 'Nama Rule berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            // $adminRole = Role::find($id);
            // $adminRole->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        session()->flash('success_message', 'Nama Rule berhasil dihapus!');
        return ['success'];
    }

    public function datatable()
    {
        $query = Role::query();

        if (request()->ajax()) {
            return Datatables::of($query)
                ->addColumn('action', function ($adminRole) {
                    $editLink = "<a href=".route('admin-roles.edit', ['admin_role' => $adminRole->id])." class='btn btn-primary btn-wth-icon icon-wthot-bg btn-sm mb-1'><span class='icon-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>ubah</span></a>";
                    return $editLink;
                })
                ->rawColumns(['action'])
                ->toJson();
        }
    }
}