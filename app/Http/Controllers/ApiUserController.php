<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GroupMenu;
use App\Helpers\RestrictedUser;
use App\Helpers\_function;
use App\Models\AdminUser;

class ApiUserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = AdminUser::get();
        return response()->json(['users' => $users], 200);
    }
}
