<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Yajra\Datatables\Datatables;
use DB;
use Excel;
use PDF;
use App\Exporter\ExportLaporan;
use Carbon\Carbon;

class AdminReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labelTable = 'Laporan Transaksi';
        $table = 'backends.includes.table-report';
        
        $routeDatatable = route('admin-reports.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        return view('backends.adminTransactions.index', 
        compact(
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'transaction_code'],
                [ 'data' => 'uuid'],
                [ 'data' => 'fullname'],
                [ 'data' => 'gender'],
                [ 'data' => 'phone'],
                [ 'data' => 'email'],
                [ 'data' => 'country_id'],
                [ 'data' => 'province_id'],
                [ 'data' => 'regency_id'],
                [ 'data' => 'district_id'],
                [ 'data' => 'sub_district_id'],
                [ 'data' => 'address'],
                [ 'data' => 'postalcode'],
                [ 'data' => 'shipping_method_id'],
                [ 'data' => 'expedition'],
                [ 'data' => 'shipping_cost'],
                [ 'data' => 'receipt_code'],
                [ 'data' => 'grand_total'],
                [ 'data' => 'payment_method_id'],
                [ 'data' => 'payment_slip'],
                [ 'data' => 'status'],
            ],
            'labelColumns' => [
                'Kode Transaksi',
                '#',
                'Nama',
                'Jenis Kelamin',
                'Telepon',
                'Email',
                'Negara',
                'Provinsi',
                'Kota',
                'Kecamatan',
                'Kelurahan',
                'Alamat',
                'Kode Pos',
                'Metode Pengiriman',
                'Ekspedisi',
                'Biaya Kirim',
                'No. Resi',
                'Total',
                'Metode Pembayaran',
                'Bukti Transfer',
                'Status',
            ]
        ];
        
        return $columns;
    }

    public function datatable()
    {
        $query = Transaction::query()->orderBy('id', 'desc');

            return Datatables::of($query)
            ->addColumn('action', function ($adminTransaction) {
                $editLink = "<a href=".route('admin-transactions.edit', ['admin_transaction' => $adminTransaction->id])." class='btn btn-primary btn-wth-Transaction Transaction-wthot-bg btn-sm mb-1'><span class='Transaction-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>detil</span></a>";

                $deleteLink = "<a href=".route('admin-transactions.destroy', ['admin_transaction' => $adminTransaction->id])." class='btn btn-danger btn-wth-Transaction Transaction-wthot-bg btn-sm delete-button'><span class='Transaction-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>hapus</span></a>";

                return $editLink . $deleteLink;
            })
            ->editColumn('uuid', function($adminTransaction){
                $print = "<a class='btn btn-warning' href=".route('admin-reports.printPdf', ['uuid' => $adminTransaction->uuid])." target='_blank'>Cetak PDF</a>";
                return $print ?? null;
            })
            ->editColumn('fullname', function($adminTransaction){
                return $adminTransaction->fullname ?? null;
            })
            ->editColumn('transaction_code', function($adminTransaction){
                return $adminTransaction->transaction_code ?? null;
            })
            ->editColumn('gender', function($adminTransaction){
                return $adminTransaction->gender == ':'?'Laki-Laki':'Perempuan' ?? null;
            })
            ->editColumn('country_id', function($adminTransaction){
                return $adminTransaction->country->name ?? null;
            })
            ->editColumn('province_id', function($adminTransaction){
                return $adminTransaction->province->name ?? null;
            })
            ->editColumn('regency_id', function($adminTransaction){
                return $adminTransaction->regency->name ?? null;
            })
            ->editColumn('district_id', function($adminTransaction){
                return $adminTransaction->district->name ?? null;
            })
            ->editColumn('sub_district_id', function($adminTransaction){
                return $adminTransaction->subDistrict->name ?? null;
            })
            ->editColumn('shipping_method_id', function($adminTransaction){
                return $adminTransaction->shippingMethod->name ?? null;
            })
            ->editColumn('payment_method_id', function($adminTransaction){
                return $adminTransaction->paymentMethod->name ?? null;
            })
            ->editColumn('grand_total', function($adminTransaction){
                return $adminTransaction->grandTotalFormat ?? null;
            })
            ->editColumn('shipping_cost', function($adminTransaction){
                return $adminTransaction->shippingCostFormat ?? null;
            })
            ->editColumn('status', function($adminTransaction){
                return $adminTransaction->displayStatus ?? null;
            })
            ->rawColumns(['uuid'])
            ->toJson();
    }

    public function print(Request $request)
    {
        $date1 = $request->date1;
        $date2 = $request->date2;
        $datePrint = Carbon::parse($date1)->format('dmY').'_'.Carbon::parse($date2)->format('dmY');
        return Excel::download(new ExportLaporan($date1, $date2), 'Laporan_' . $datePrint . '.xls');
    }

    public function printPdf($uuid)
    {
        $data['transaction'] = Transaction::where('uuid', '=', $uuid)->first();
        $now = Carbon::now();

        return PDF::loadView('backends.adminReports.printPdf', $data)->setOptions(['dpi' => 150, 'defaultFont' => 'arial', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->stream('print_'.$data['transaction']->transaction_code.'_'.Carbon::parse($now)->format('dmY').'_'.'.pdf');
    }
}