<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Yajra\Datatables\Datatables;
use DB;

class AdminTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labelTable = 'Daftar Transaksi';
        $table = 'backends.includes.table';
        
        $routeDatatable = route('admin-transactions.datatable');
        $datatableColumns = $this->getDatatableColumns();
        $fileUpload = false;

        return view('backends.adminTransactions.index', 
        compact(
            'labelTable',
            'table',
            'routeDatatable',
            'datatableColumns',
            'fileUpload',
        ));
    }

    private function getDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'fullname'],
                [ 'data' => 'transaction_code'],
                [ 'data' => 'gender'],
                [ 'data' => 'phone'],
                [ 'data' => 'email'],
                [ 'data' => 'country_id'],
                [ 'data' => 'province_id'],
                [ 'data' => 'regency_id'],
                [ 'data' => 'district_id'],
                [ 'data' => 'sub_district_id'],
                [ 'data' => 'address'],
                [ 'data' => 'postalcode'],
                [ 'data' => 'shipping_method_id'],
                [ 'data' => 'expedition'],
                [ 'data' => 'shipping_cost'],
                [ 'data' => 'receipt_code'],
                [ 'data' => 'grand_total'],
                [ 'data' => 'payment_method_id'],
                [ 'data' => 'payment_slip'],
                [ 'data' => 'status'],
                [ 'data' => 'action', 'orderable' => false, 'searchable' => false ],
            ],
            'labelColumns' => [
                'Nama',
                'Kode Transaksi',
                'Jenis Kelamin',
                'Telepon',
                'Email',
                'Negara',
                'Provinsi',
                'Kota',
                'Kecamatan',
                'Kelurahan',
                'Alamat',
                'Kode Pos',
                'Metode Pengiriman',
                'Ekspedisi',
                'Biaya Kirim',
                'No. Resi',
                'Total',
                'Metode Pembayaran',
                'Bukti Transfer',
                'Status',
                'Aksi'
            ]
        ];
        
        return $columns;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $adminTransaction = new Transaction;
    //         $adminTransaction = $adminTransaction->saveFromRequest($request);
    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollback();
    //     }

    //     return redirect()->route('admin-transactions.index')
    //         ->with('success_message', 'Transactions successfully created');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataReference = Transaction::find($id);
        $dataReferenceId = $dataReference->id;

        $form = 'backends.adminTransactions.form';
        $action = 'backends.includes.edit';
        $route = 'admin-transactions.update';
        $routeMethod = 'PATCH';

        $label = 'Transaksi';
        $labelTable = 'Daftar Pesanan';
        $table = 'backends.includes.table';

        $routeDatatable = route('admin-transaction-details.datatable', ['transaction_id' => $id]);
        $datatableColumns = $this->getDetailDatatableColumns();
        
        $fileUpload = false;

        return view('backends.adminTransactions.index', 
        compact(
            'dataReference',
            'dataReferenceId',
            'form',
            'action',
            'route',
            'routeMethod',
            'label',
            'labelTable',
            'table',
            'routeDatatable',
            'fileUpload',
            'datatableColumns'
        ));
    }

    private function getDetailDatatableColumns()
    {
        $columns = (object)[
            'dataColumns' => [
                [ 'data' => 'product_detail_id'],
                [ 'data' => 'product_name'],
                [ 'data' => 'product_detail'],
                [ 'data' => 'qty'],
                [ 'data' => 'total'],
            ],
            'labelColumns' => [
                'Produk',
                'Nama Produk',
                'Keterangan',
                'Jumlah',
                'Total',
            ]
        ];
        
        return $columns;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $adminTransaction = Transaction::find($id);

            $adminTransaction = $adminTransaction->saveFromRequest($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin-transactions.edit', ['admin_transaction' => $id])
            ->with('success_message', 'Transaksi berhasil diperbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $adminTransaction = Transaction::find($id);
    //         $adminTransaction->delete();
    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollback();
    //     }
    //     session()->flash('success_message', 'Transaksi berhasil dihapus');
    //     return ['success'];
    // }

    public function datatable()
    {
        $query = Transaction::query()->orderBy('id', 'desc');

            return Datatables::of($query)
            ->addColumn('action', function ($adminTransaction) {
                $editLink = "<a href=".route('admin-transactions.edit', ['admin_transaction' => $adminTransaction->id])." class='btn btn-primary btn-wth-Transaction Transaction-wthot-bg btn-sm mb-1'><span class='Transaction-label'><i class='fa fa-edit'></i> </span><span class='btn-text'>detil</span></a>";

                $deleteLink = "<a href=".route('admin-transactions.destroy', ['admin_transaction' => $adminTransaction->id])." class='btn btn-danger btn-wth-Transaction Transaction-wthot-bg btn-sm delete-button'><span class='Transaction-label'><i class='fa fa-trash'></i> </span><span class='btn-text'>hapus</span></a>";

                return $editLink . $deleteLink;
            })
            ->editColumn('fullname', function($adminTransaction){
                return $adminTransaction->fullname ?? null;
            })
            ->editColumn('transaction_code', function($adminTransaction){
                $detail = "<a href=".route('admin-transactions.edit', ['admin_transaction' => $adminTransaction->id]).">".$adminTransaction->transaction_code."</a>";

                return $detail ?? null;
            })
            ->editColumn('gender', function($adminTransaction){
                return $adminTransaction->gender == ':'?'Laki-Laki':'Perempuan' ?? null;
            })
            ->editColumn('country_id', function($adminTransaction){
                return $adminTransaction->country->name ?? null;
            })
            ->editColumn('province_id', function($adminTransaction){
                return $adminTransaction->province->name ?? null;
            })
            ->editColumn('regency_id', function($adminTransaction){
                return $adminTransaction->regency->name ?? null;
            })
            ->editColumn('district_id', function($adminTransaction){
                return $adminTransaction->district->name ?? null;
            })
            ->editColumn('sub_district_id', function($adminTransaction){
                return $adminTransaction->subDistrict->name ?? null;
            })
            ->editColumn('shipping_method_id', function($adminTransaction){
                return $adminTransaction->shippingMethod->name ?? null;
            })
            ->editColumn('payment_method_id', function($adminTransaction){
                return $adminTransaction->paymentMethod->name ?? null;
            })
            ->editColumn('grand_total', function($adminTransaction){
                return $adminTransaction->grandTotalFormat ?? null;
            })
            ->editColumn('shipping_cost', function($adminTransaction){
                return $adminTransaction->shippingCostFormat ?? null;
            })
            ->editColumn('status', function($adminTransaction){
                return $adminTransaction->displayStatus ?? null;
            })
            ->rawColumns(['transaction_code', 'action'])
            ->toJson();
    }
}