<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class GroupMenu extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const TYPE_LABEL = 0;
    const TYPE_ACTIVE_MENU = 1;

    const CATEGORY_PARENT_LABEL = 0;
    const CATEGORY_EDIT_ONLY = 1;
    const CATEGORY_CRUD = 2;

    const UNPUBLISH = 0;
    const PUBLISHED = 1;

    const DESCRIPTION_TRUE = 1;
    const DESCRIPTION_FALSE = 0;

    const SHOW_LABEL_TRUE = 1;
    const SHOW_LABEL_FALSE = 0;

    protected $table = 'group_menus';
    protected $fillable = [
        'parent_id',
        'label',
        'url',
        'icon',
        'type',
        'category',
        'media_category',
        'publish',
        'order',
        'show_label',
        'description_column',
        'image_width',
        'image_height',
        'video_width',
        'video_height',
        'file_view_id'
    ];

    public static function asDropdownOptions()
    {
        return self::whereNull('parent_id')->pluck('label', 'id')->all();
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function getDisplayTypeAttribute()
    {
        $type = 'Label';

        if($this->type == self::TYPE_ACTIVE_MENU){
            $type = 'Active Menu';
        }

        return $type;
    }

    public function getDisplayCategoryAttribute()
    {
        $category = 'Parent Label';

        if($this->category == self::CATEGORY_EDIT_ONLY){
            $category = 'Edit Only';
        }else if($this->category == self::CATEGORY_CRUD){
            $category = 'CRUD';
        }

        return $category;
    }

    public function getDisplayPublishAttribute()
    {
        $publish = 'Unpublish';

        if($this->publish == self::PUBLISHED){
            $publish = 'Published';
        }

        return $publish;
    }

    public function getDisplayShowLabelAttribute()
    {
        $label = 'Show Label';

        if($this->show_label == self::SHOW_LABEL_FALSE){
            $label = 'Hide Label';
        }

        return $label;
    }

    public function getDisplayDescriptionAttribute()
    {
        $description = 'Use Description';

        if($this->description_column == self::DESCRIPTION_FALSE){
            $description = 'No Description';
        }

        return $description;
    }

    public function scopePublishedMenu($q)
    {
        return $q->where('publish', '=', self::PUBLISHED);
    }

    public function scopeUrlNotNull($q)
    {
        return $q->WhereNotNull('url')
        ->where('url', '!=', '');
    }

    public function getCategoryMenuAttribute()
    {
        $crud = false;

        if($this->category == self::CATEGORY_CRUD){
            $crud = true;
        }

        return $crud;
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        $data['url'] = Str::of($request->url)->lower();
        return $data;
    }

}