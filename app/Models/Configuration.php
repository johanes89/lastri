<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Media;
use Storage;
use Html;

class Configuration extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder-480.jpg';
    const DEFAULT_PATH_IMAGE_LOGO = 'images/no-image-circle-240.png';
    const DEFAULT_PATH_IMAGE_SMALL_LOGO = 'images/no-image-circle-30.png';

    protected $table = 'configurations';
    protected $fillable = [
        'name',
        'logo',
        'small_logo',
        'icon',
        'payment_information',
        'keywords',
        'official_email',
        'official_phone',
        'official_address',
        'copyright_label',
        'copyright',
        'copyright_url',
        'year',
        'version',
    ];

    protected $appends = [
        'logo_url', 
        'small_logo_url', 
        'icon_url'
    ];

    public function getLogoThumbAttribute()
    {
        return Html::image($this->logoUrl, null, ['height' => '50px']) ?? null;
    }

    public function getSmallLogoThumbAttribute()
    {
        return Html::image($this->smallLogoUrl, null, ['height' => '50px']) ?? null;
    }

    public function getIconThumbAttribute()
    {
        return Html::image($this->iconUrl, null, ['height' => '50px']) ?? null;
    }

    public function getLogoUrlAttribute()
    {
        if ((!empty($this->logo)) && Media::fileExists($this->logo)){

            $file = 'storage/'.$this->logo;

        } else {

            $file = self::DEFAULT_PATH_IMAGE_LOGO;

        }
        return Media::getImageUrl($file);
    }

    public function getSmallLogoUrlAttribute()
    {
        if ((!empty($this->small_logo)) && Media::fileExists($this->small_logo)){

            $file = 'storage/'.$this->small_logo;

        } else {

            $file = self::DEFAULT_PATH_IMAGE_SMALL_LOGO;

        }
        return Media::getImageUrl($file);
    }

    public function getIconUrlAttribute()
    {
        if ((!empty($this->icon)) && Media::fileExists($this->icon)){

            $file = 'storage/'.$this->icon;

        } else {

            $file = self::DEFAULT_PATH_IMAGE_SMALL_LOGO;

        }
        return Media::getImageUrl($file);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        $path = "configurations";

        if($request->hasFile('logo')){
            $image_url = Media::imageUpload($path, 320, 320, $request->file('logo'));
            $data['logo'] = $image_url;
        }

        if($request->hasFile('small_logo')){
            $image_url = Media::imageUpload($path, 160, 160, $request->file('small_logo'));
            $data['small_logo'] = $image_url;
        }

        if($request->hasFile('icon')){
            $image_url = Media::imageUpload($path, 50, 50, $request->file('icon'));
            $data['icon'] = $image_url;
        }

        return $data;
    }

}