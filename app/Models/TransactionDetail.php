<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;

class TransactionDetail extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table="transaction_details";
    protected $fillable =[
        'transaction_id',
        'product_detail_id',
        'qty',
        'total'
    ];

    protected $appends = ['total_format'];

    public function productDetail()
    {
        return $this->belongsTo(ProductDetail::class, 'product_detail_id');
    }

    public function getTotalFormatAttribute()
    {
        return Product::formatRupiah($this->total);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        return $data;
    }
}
