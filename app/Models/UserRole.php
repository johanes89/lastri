<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'user_roles';
    protected $fillable = [
        'id',
        'user_id',
        'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function user()
    {
        return $this->belongsTo(AdminUser::class, 'user_id');
    }

}