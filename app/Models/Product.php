<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers\Media;
use Html;
use Illuminate\Support\Str;
use App\Traits\Uuid;

class Product extends Model
{
    use FormAccessible;
    use SaveableTrait;
    use Uuid;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';
    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $table="products";
    protected $fillable =[
        'uuid',
        'category_id',
        'name',
        'price',
        'discount',
        'discount_type',
        'description',
        'status'
    ];

    protected $appends = [
        'first_image_detail', 
        'first_id', 
        'category_name', 
        'option_product_detail', 
        'price_format', 
        'count_discount', 
        'price_after_discount', 
        'count_discount_format', 
        'price_after_discount_format'
    ];

    public static function scopeActive($q)
    {
        return $q->where(
            'status',
            '=',
            self::ACTIVE
        );
    }

    public function getDisplayStatusAttribute()
    {
        $status = 'Inactive';
        if($this->status == self::ACTIVE){
            $status = 'Active';
        }

        return $status;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function productDetail()
    {
        return $this->hasMany(ProductDetail::class, 'product_id');
    }

    public function getFirstImageDetailAttribute()
    {
        return (!empty($this->productDetail->first())?$this->productDetail->first()->fileUrl:self::DEFAULT_PATH_IMAGE);
    }

    public function getFirstIdAttribute()
    {
        return (!empty($this->productDetail->first())?$this->productDetail->first()->id:"");
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }

    public static function formatRupiah($value)
    {
        $hasil_rupiah = "Rp " . number_format($value, 2, ',', '.');
        return $hasil_rupiah;
    }

    public function getCountDiscountAttribute()
    {
        $discount = $this->discount;
        if($this->discount_type == 'percentage'){
            $discount = $this->price*($this->discount/100);
        }
        return $discount;
    }

    public function getPriceAfterDiscountAttribute()
    {
        $priceAfterDiscount = $this->price - $this->countDiscount;
        return $priceAfterDiscount;
    }

    public function getCountDiscountFormatAttribute()
    {
        return $this->formatRupiah($this->countDiscount);
    }

    public function getPriceAfterDiscountFormatAttribute()
    {
        return $this->formatRupiah($this->priceAfterDiscount);
    }

    public function getPriceFormatAttribute()
    {
        return $this->formatRupiah($this->price);
    }

    public function getOptionProductDetailAttribute()
    {
        return $this->productDetail->map(function($detail){
            return [
                'id' => $detail->id,
                'text' => $detail->productDetailName,
                'stock' => $detail->qty,
                'image' => $detail->fileUrl,
            ];
        });
    }
    
    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        return $data;
    }
}
