<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers\Media;
use Html;
use App\Traits\Uuid;

class Member extends Model
{
    use FormAccessible;
    use SaveableTrait;
    use Uuid;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';
    const DEFAULT_PATH_IMAGE_PHOTO_IDENTITY = 'images/image-placeholder.jpg';

    protected $table="members";
    protected $fillable =[
        'uuid',
        'user_id',
        'member_type_id',
        'code',
        'prefix_id',
        'firstname',
        'lastname',
        'identity_type_id',
        'identity_number',
        'gender',
        'phone',
        'secondary_phone',
        'email',
        'birthplace',
        'birthdate',
        'origin_country',
        'origin_city',
        'origin_address',
        'origin_postal_code',
        'education_id',
        'occupation_id',
        'religion_id',
        'marital_status_id',
        'hobby_id',
        'country_id',
        'province_id',
        'regency_id',
        'district_id',
        'sub_district_id',
        'address',
        'postal_code',
        'note',
        'photo_url',
        // 'photo_identity_url',
    ];

    public function user()
    {
        return $this->belongsTo(AdminUser::class, 'user_id');
    }

    public function memberType()
    {
        return $this->belongsTo(MemberType::class, 'member_type_id');
    }

    public function prefix()
    {
        return $this->belongsTo(Prefix::class, 'prefix_id');
    }
    
    public function identityType()
    {
        return $this->belongsTo(IdentityType::class, 'identity_type_id');
    }

    public function education()
    {
        return $this->belongsTo(Education::class, 'education_id');
    }

    public function occupation()
    {
        return $this->belongsTo(Occupation::class, 'occupation_id');
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'religion_id');
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class, 'marital_status_id');
    }
    
    public function hobby()
    {
        return $this->belongsTo(Hobby::class, 'hobby_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function subDistrict()
    {
        return $this->belongsTo(SubDistrict::class, 'sub_district_id');
    }

    public static function asDropdownOptions()
    {
        return self::pluck('firstname', 'id')->all();
    }

    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '50px']) ?? null;
    }

    public function getFileImageUrlAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '100px']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->photo_url)) && Media::fileExists($this->photo_url)){

            $file = 'storage/'.$this->photo_url;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        $path = 'members';
        if($request->hasFile('photo_url')){
            
            $image_url = Media::imageUpload($path, 640, 640, $request->file('photo_url'));
            $data['photo_url'] = $image_url;
        }

        // if($request->hasFile('photo_identity_url')){
            
        //     $image_url_photo_identity = Media::imageUpload($path, 640, 640, $request->file('photo_identity_url'));
        //     $data['photo_identity_url'] = $image_url_photo_identity;
        // }

        return $data;
    }
}
