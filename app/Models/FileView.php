<?php

namespace App\Models;

use App\Traits\SaveableTrait;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FileView extends Model
{
    use FormAccessible;
    use SaveableTrait;

    protected $table = 'file_views';
    protected $fillable = [
        'label',
        'filename',
    ];

    public static function asDropdownOptions()
    {
        return self::pluck('label', 'id')->all();
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');
        return $data;
    }

}