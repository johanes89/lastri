<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $table= 'ref_religions';
    protected $fillable = [
        'name'
    ];
    
    public static function asDropdownOptions()
    {
        return self::pluck('name', 'id')->all();
    }

}
