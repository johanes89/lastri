<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SaveableTrait;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers\Media;
use Html;
use Illuminate\Support\Str;

class ProductDetail extends Model
{
    use FormAccessible;
    use SaveableTrait;

    const DEFAULT_PATH_IMAGE = 'images/image-placeholder.jpg';

    protected $table="product_details";
    protected $fillable =[
        'product_id',
        'color',
        'variation',
        'qty',
        'photo_url'
    ];

    protected $appends = ['file_url', 'product_detail_name'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getFileImageThumbAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '50px']) ?? null;
    }

    public function getFileImageUrlAttribute()
    {
        return Html::image($this->fileUrl, null, ['height' => '100px']) ?? null;
    }

    public function getFileUrlAttribute()
    {
        if ((!empty($this->photo_url)) && Media::fileExists($this->photo_url)){

            $file = 'storage/'.$this->photo_url;

        } else {

            $file = self::DEFAULT_PATH_IMAGE;

        }
        return Media::getImageUrl($file);
    }

    public function getProductDetailNameAttribute()
    {
        return "Warna: ".$this->color.", Variasi: ".$this->variation;
    }

    public function composeDataFromRequest($request)
    {
        $data = $request->except('_token');

        $path = 'products';
        if($request->hasFile('photo_url')){
            
            $image_url = Media::imageUploadCrop ($path, 640, 640, $request->file('photo_url'), 640, 640);
            $data['photo_url'] = $image_url;
        }

        return $data;
    }
}
